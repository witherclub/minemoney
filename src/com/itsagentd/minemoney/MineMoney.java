package com.itsagentd.minemoney;

import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.itsagentd.minemoney.listeners.LBlock;
import com.itsagentd.minemoney.util.ConfigFile;

public class MineMoney extends JavaPlugin {
	
	public final static Logger log = Logger.getLogger("Minecraft");
	public static FileConfiguration config;
	
	@Override
	public void onEnable() {
		
		ConfigFile configfile = new ConfigFile();
		configfile.loadConfig("config.yml", this);
		
		config = configfile.getConfig();
		
		MineEconomy.initializeEconomy(getServer());
		
		getServer().getPluginManager().registerEvents(new LBlock(), this);
	}
	
	@Override
	public void onDisable() {
		
	}
}
