package com.itsagentd.minemoney.listeners;

import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.itsagentd.minemoney.MineEconomy;
import com.itsagentd.minemoney.MineMoney;

import net.milkbowl.vault.economy.Economy;

public class LBlock implements Listener {

	@EventHandler
	public void onBlockBreak( BlockBreakEvent e ) {
		
		Player player = e.getPlayer();
		Block block = e.getBlock();
		
		Economy econ = MineEconomy.getEcon();
		double amount = 0;
		FileConfiguration config = MineMoney.config;
		
		if (player == null)
			return;
		
		if (player.getGameMode() == GameMode.CREATIVE)
			return;
		
		switch (block.getType()) {
		case COAL_ORE:
			amount = config.getDouble("amounts.coal_ore");
			break;
			
		case IRON_ORE:
			amount = config.getDouble("amounts.iron_ore");
			break;
			
		case GOLD_ORE:
			amount = config.getDouble("amounts.gold_ore");
			break;
			
		case REDSTONE_ORE:
			amount = config.getDouble("amounts.redstone_ore");
			break;
			
		case GLOWING_REDSTONE_ORE:
			amount = config.getDouble("amounts.redstone_ore");
			break;
			
		case LAPIS_ORE:
			amount = config.getDouble("amounts.lapis_ore");
			break;
			
		case DIAMOND_ORE:
			amount = config.getDouble("amounts.diamond_ore");
			break;
			
		case EMERALD_ORE:
			amount = config.getDouble("amounts.emerald_ore");
			break;
		
		case QUARTZ_ORE:
			amount = config.getDouble("amounts.quartz_ore");
			break;
			
		default:
			break;
		}
		
		if (amount > 0) {
			econ.depositPlayer(player, amount);

			if (config.getBoolean("general.consoleoutput"))
				MineMoney.log.info(player.getDisplayName() + " received " + amount + " currency for mining.");
		}
	}
}
