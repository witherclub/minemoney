package com.itsagentd.minemoney.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.itsagentd.minemoney.MineMoney;

public class ConfigFile {
	
	private static FileConfiguration config;
	private static File file;
	private String filename;
	private MineMoney minemoney;

	public void loadConfig(String filename, MineMoney minemoney) {
		
		file = new File(minemoney.getDataFolder(), filename);
		this.filename = filename;
		this.minemoney = minemoney;
		
		if (!file.exists())
			createConfig(filename, minemoney);
		
		config = new YamlConfiguration();
		try {
			
			config.load(file);
			MineMoney.log.info("[MobMoney] Configuration file loaded successfully!");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	private void createConfig(String filename, MineMoney minemoney) {
		
		file.getParentFile().mkdirs();
		copy(minemoney.getResource(filename));
	}

	private void copy(InputStream resource) {
		
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			
			while((len = resource.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			
			out.close();
			resource.close();
			
			MineMoney.log.info("[MineMoney] Configuration file missing. Copied defaults!");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public FileConfiguration getConfig() {
		
		return config;
	}
	
	public void saveConfig() {
		
		try {
			
			config.save(file);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public void reloadConfig() {
		loadConfig(this.filename, this.minemoney);
	}
}
